<?php
/**
 * @file
 *
 * The admin/config/people/simplesamlphp_idp_settings form.
 *

/**
 * The callback function for admin/config/people/simplesamlphp_idp_settings.
 */
function simplesamlphp_idp_settings_form() {

  $form = array();

  $form['simplesamlphp_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('SimpleSAMLphp details'),
  );

  $form['simplesamlphp_details']['simplesamlphp_idp_base_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('The base directory to simplesamlphp configured as an idp (identity provider)'),
    '#description' => t('The base directory to a simplesamlphp installation configured as an idp (full path without trailing slash at the end, default: /var/simplesamlphp )'),
    '#default_value' => variable_get('simplesamlphp_idp_base_dir', '/var/simplesamlphp'),
  );

  $form['simplesamlphp_details']['simplesamlphp_idp_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The full URL to simplesamlphp configured as an idp (identity provider).'),
    '#description' => t('The full URL including http:// or https:// always end with a trailing slash. If simplesamlphp idp is hosted at the same domain as your drupal site, this value should probably be your domain followed bij simplesaml/ (e.g. https://example.com/simplesaml/ when your drupal site is at https://example.com) but it can be any full URL to your idp.'),
    '#default_value' => variable_get('simplesamlphp_idp_base_url', 'https://changeme.com/simplesaml/'),
  );

  $form['simplesamlphp_details']['simplesamlphp_idp_auth_source'] = array(
    '#type' => 'textfield',
    '#title' => t('The authentication source used by simplesamlphp configured as an idp (identity provider)'),
    '#description' => t('This should be the name of the authentication source used in metadata/saml20-idp-hosted.php under "auth" and should also be configured in config/authsources.php'),
    '#default_value' => variable_get('simplesamlphp_idp_auth_source', 'drupalas-example'),
  );

  return system_settings_form($form);
}

/**
 * The validation callback function for admin/config/people/auth_db_settings
 */
function simplesamlphp_idp_settings_form_validate($form, &$form_state) {

  if ($form['simplesamlphp_details']['simplesamlphp_idp_base_dir']['#value'] == '') {
    form_set_error('simplesamlphp_idp_base_dir', t('The simplesamlphp base directory cannot be empty'));
  }

  if ($form['simplesamlphp_details']['simplesamlphp_idp_base_url']['#value'] == '') {
    form_set_error('simplesamlphp_idp_base_url', t('The simplesamlphp base URL cannot be empty'));
  }

  if ($form['simplesamlphp_details']['simplesamlphp_idp_auth_source']['#value'] == '') {
    form_set_error('simplesamlphp_idp_auth_source', t('The authentication source cannot be empty'));
  }
}