<?php


/**
 * Function to execute when a saml login request is detected and the user is logged in to Drupal.
 *
 * Should only be called when a user is logged in
 *
 * @param object @user current user that is logged in and allowed to use saml idp
 *
 * @param string $stateId_load current state id collected from get value
 *
 * @param string $returnTo address to return to after authentication steps
 *
 * Redirects if saml login detected, returns TRUE if a normal login is continuing
 */
function _simplesamlphp_idp_login($user, $stateId_load, $returnTo) {

  //make sure the user is logged in, this is a bit redundant at the login hook
  // but you never know
  if (user_is_logged_in()) {

    //make sure the user is allowed to use saml idp
    if (user_access('use saml idp', $user)) {

      //load the simplesamlphp config values
      module_load_include('php', 'simplesamlphp_idp', 'simplesamlphp_idp_get_config');
      $sspIdpConfigValues = _simplesamlphp_idp_get_sspidpconfig();

      if (!is_array($sspIdpConfigValues)) {

        watchdog('simplesamlphp_idp', t('Unable to load simplesamlphp configuration values during the login process, check the other log messages'));
        return;

      }

      //load the simplesamlphp autoload.php file
      require_once($sspIdpConfigValues['simplesamlphp_dir'] . '/lib/_autoload.php');

      //load the state
      $state = SimpleSAML_Auth_State::loadState($stateId_load, 'drupalas_login_stage_one');

      //make sure the authentication source used (for the identity provider) is the same in drupal and simplesamlphp
      if ($state['drupalas_AuthID'] != $sspIdpConfigValues['simplesamlphp_idp_auth_source']) {

        watchdog('simplesamlphp_idp', t('Error authentication source name mismatch: please use the same authentication source (name) both in the drupal module and in authsources.php.'));
        return;

      }

      //generate a secret salt just for this login session
      $sessionSecretSalt = NULL;
      $sessionSecretSalt = base64_encode(drupal_random_bytes(32));

      if (!strlen($sessionSecretSalt)) {

        watchdog('simplesamlphp_idp', t('Critical error while generating salt: this should not have happened.'));
        return;

      }

      //generate a hash based on the session salt to embed in the link
      $sessionHash = NULL;
      $sessionHash = hash($sspIdpConfigValues['hash_algorithm'], $sessionSecretSalt . $user->uid);


      //store session salt in state array so simplesamlphp can check the hash in the next step
      $state['drupal_login_salt'] = $sessionSecretSalt;

      //clear the sessionSecretSalt for security reasons
      $sessionSecretSalt = NULL;

      //store the uid of the authenticated user in the state array
      $state['drupal_uid'] = $user->uid;


      //check is configuration dictates the use of a (bridge) cookie
      if ($sspIdpConfigValues['use_cookie'] == TRUE) {

        //we are going to use a cookie

        //generate a salt just for this cookie
        $sessionCookieSalt = NULL;
        $sessionCookieSalt = base64_encode(drupal_random_bytes(16));

        if (!strlen($sessionCookieSalt)) {

          watchdog('simplesamlphp_idp', t('Critical error while generating salt: this should not have happened.'));
          return;

        }

        //store cookie salt in state array so simplesamlphp can check the hash in the next step
        $state['drupal_cookie_salt'] = $sessionCookieSalt;

        //check if the cookie will be secure
        if ($sspIdpConfigValues['cookie_secure'] == TRUE) {

          //a secure cookie (https:// only) will be used so lets set it
          //there should be a check before setting this cookie checking if this drupal site and the simplesamlphp idp are indeed securely connected (using https://)
          setcookie($sspIdpConfigValues['cookie_name'], hash($sspIdpConfigValues['hash_algorithm'], $sspIdpConfigValues['secretsalt'] . $user->uid . $sessionCookieSalt), time() + 60, $sspIdpConfigValues['cookie_path'], $sspIdpConfigValues['cookie_domain'], TRUE);


        } else {

          //a non secure (http:// and https://) cookie will be used so lets set it
          setcookie($sspIdpConfigValues['cookie_name'], hash($sspIdpConfigValues['hash_algorithm'], $sspIdpConfigValues['secretsalt'] . $user->uid . $sessionCookieSalt), time() + 60, $sspIdpConfigValues['cookie_path']);

        }

        //clear the cookie salt
        $sessionCookieSalt = NULL;

      }

      $stateId_new = SimpleSAML_Auth_State::saveState($state, 'drupalas_login_stage_two', TRUE);

      //destroy ste state array just to be sure
      $state = NULL;

      //get the returnTo link value and append the new stateId and generated hash
      $goto_with_stateId = $returnTo . '?state=' . $stateId_new . '&key=' . $sessionHash;

      //clear the sspIdpConfigValues array for security
      $sspIdpConfigValues = NULL;

      //go to the new goto address
      drupal_goto($goto_with_stateId);

      //this return should never be reached because of the goto above
      return;


    } else {

      //set error message telling user is not allowed to use SAML
      $message = t('You are currently not allowed to use SAML');
      drupal_set_message($message, 'warning');

      //nothing more to do, just return
      return;

    }

  }

}

/**
 * Function for logging out the user from simplesamlphp, supporting single logout
 *
 * @param boolean @ssp_ini simplesamlphp initiated logout yes or no
 *  TRUE completes (single) logout initiated by simplesamlphp(idp)
 *  FALSE initiates SLO at the simplesaml idp if authenticated
 */
function _simplesamlphp_idp_logout($ssp_init = FALSE, $stateId_load = NULL) {

  //load the simplesamlphp config values
  module_load_include('php', 'simplesamlphp_idp', 'simplesamlphp_idp_get_config');
  $sspIdpConfigValues = _simplesamlphp_idp_get_sspidpconfig();

  if (!is_array($sspIdpConfigValues)) {

    watchdog('simplesamlphp_idp', t('Unable to load simplesamlphp configuration values during the logout process, check the other log messages'));
    return;

  }

  if ($sspIdpConfigValues['use_cookie'] == TRUE) {

    //check if the authentication bridge cookie (still) exists
    if (isset($_COOKIE[$sspIdpConfigValues['cookie_name']])) {

      //the cookie still exists lets delete it
      //deleting the cookie by setting a new empty one with a negative time validity
      setcookie($sspIdpConfigValues['cookie_name'], '', -3600, $sspIdpConfigValues['cookie_path']);

    }

  }

  //load the simplesamlphp autoload.php file
  require_once($sspIdpConfigValues['simplesamlphp_dir'] . '/lib/_autoload.php');

  //check what kind of logout to run
  if ($ssp_init == TRUE) {

    //logout was started by simplesamlphp, lets continue

    //check if the user is still logged in to drupal
    if (user_is_logged_in()) {

      //the user is still logged in to drupal

      // destroy the user session (the drupal functions for destroying a session
      // doesnt actually destroy the session, so using destroy_session()
      // instead
      session_destroy();

    }

    //load the state
    $state = SimpleSAML_Auth_State::loadState($stateId_load, 'drupalas_logout');

    //load the simplesamlphp auth source
    $authSource = SimpleSAML_Auth_Source::getById($sspIdpConfigValues['simplesamlphp_idp_auth_source']);

    //clear the sspIdpConfigValues array for security
    $sspIdpConfigValues = NULL;

    //complete the (single) logout, this function should redirect
    $authSource->completeLogout($state);

  } else {

    //logout started by drupal, lets check if there is a saml session going on

    //create simplesamlphp authentication object
    $auth = new SimpleSAML_Auth_Simple($sspIdpConfigValues['simplesamlphp_idp_auth_source']);

    // check if user is saml authenticated
    // somehow isAuthenticated does not work as expected when using == TRUE
    // so using == 1 insted, which gives the desired result
    if ($auth->isAuthenticated() == 1) {

      //the user has a saml session, lets start a single logout

      //we have the ability to redirect after logout, so we generate a redirect link
      //right now this just returns to the drupal site base URL
      Global $base_url;
      $slo_returnTo = urlencode($base_url);

      //create the SLO link
      $slo_goto = $sspIdpConfigValues['simplesamlphp_idp_base_url'] . 'saml2/idp/SingleLogoutService.php?ReturnTo=' . $slo_returnTo;

      //clear the sspIdpConfigValues array for security
      $sspIdpConfigValues = NULL;

      //redirect to the SLO link
      drupal_goto($slo_goto);

    } else {

      //clear the sspIdpConfigValues array for security
      $sspIdpConfigValues = NULL;

      //just return if no saml session has been found
      return null;

    }

  }

}