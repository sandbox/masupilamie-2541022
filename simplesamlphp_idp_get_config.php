<?php
/**
 * Function to get main config values needed from simplesamlphp
 */
function _simplesamlphp_idp_get_sspidpconfig() {

  //clear and declare sspIdpConfigValues array just to be sure and for security reasons
  $sspIdpConfigValues = NULL;
  $sspIdpConfigValues = array();
  $sspIdpAuthConfigValues = NULL;



  //get the auth source configured
  $sspIdpConfigValues['simplesamlphp_idp_auth_source'] = variable_get('simplesamlphp_idp_auth_source', NULL);

  //check if a value has been found
  if (isset($sspIdpConfigValues['simplesamlphp_idp_auth_source'])) {

    if (!strlen($sspIdpConfigValues['simplesamlphp_idp_auth_source'])) {

      watchdog('simplesamlphp_idp', t('Error: empty configuration parameter, please set simplesamlphp(idp) authentication source in the drupal module settings.'));
      return;

    }

  } else {

    watchdog('simplesamlphp_idp', t('Error: missing configuration parameter, please set simplesamlphp(idp) authentication source in the drupal module settings.'));
    return;

  }



  //get the simplesamlphp idp full base url
  $sspIdpConfigValues['simplesamlphp_idp_base_url'] = variable_get('simplesamlphp_idp_base_url', NULL);

  //check if a value has been found
  if (isset($sspIdpConfigValues['simplesamlphp_idp_base_url'])) {

    if (!strlen($sspIdpConfigValues['simplesamlphp_idp_base_url'])) {

      //set error to log about empty value
      watchdog('simplesamlphp_idp', t('Error: empty configuration parameter, please set simplesamlphp(idp) base URL the drupal module settings.'));
      return;

    }

  } else {

    //no simplesamlphp_idp_base_url setting found
    watchdog('simplesamlphp_idp', t('Error: missing configuration parameter, please set simplesamlphp(idp) base URL.'));
    return;

  }



  //get the simplesamlphp base directory
  $sspIdpConfigValues['simplesamlphp_dir'] = variable_get('simplesamlphp_idp_base_dir', NULL);

  //check if a value has been found
  if (isset($sspIdpConfigValues['simplesamlphp_dir'])) {

    if (!strlen($sspIdpConfigValues['simplesamlphp_dir'])) {
      watchdog('simplesamlphp_idp', t('Error: empty configuration parameter, please set simplesamlphp(idp) base directory in the drupal module settings.'));
      return;

      //check if directory actually exist
    } else if (!file_exists($sspIdpConfigValues['simplesamlphp_dir'])) {

      watchdog('simplesamlphp_idp', t('Simplesamlphp(idp) could not be found at the configured directory. Please configure the correct simplesamlphp(idp) base directory in the drupal module settings.'));
      return;

    }

  } else {

    watchdog('simplesamlphp_idp', t('Error: missing configuration parameter, please set simplesamlphp(idp) base directory in the drupal module settings.'));
    return;

  }



  //check if the autoload file exists and load it when it does
  if (!file_exists($sspIdpConfigValues['simplesamlphp_dir'] . '/lib/_autoload.php')) {

    watchdog('simplesamlphp_idp', t('Error: unable to autoload simplesamlphp, is your simplesamlphp installation corrupted?.'));
    return;

  } else {

    //load the simplesamlphp autoload.php file
    require_once($sspIdpConfigValues['simplesamlphp_dir'] . '/lib/_autoload.php');

  }



  //create simplesamlphp configuration object to load some configuration values
  $sspIdpConfig = SimpleSAML_Configuration::getInstance();

  //set error message if loading of configuration object failed
  if (!is_object($sspIdpConfig)) {
    watchdog('simplesamlphp_idp', t('Error connecting to simplesamlphp: is simplesamlphp(idp) installed at the given base directory?'));
    return;
  }

  //get the base url path for use in cookie from the simplesamlphp config.php file
  $sspIdpConfigValues['cookie_path'] = '/' . $sspIdpConfig->getValue('baseurlpath');

  //get the secret salt from the simplesaml configuration file for later use
  $sspIdpConfigValues['secretsalt'] = $sspIdpConfig->getValue('secretsalt');

  if (isset($sspIdpConfigValues['secretsalt'])) {

    if (!strlen($sspIdpConfigValues['secretsalt'])) {

      watchdog('simplesamlphp_idp', t('Error empty configuration value: please set a value for secretsalt in config.php'));
      return;

    } else if ($sspIdpConfigValues['secretsalt'] == 'defaultsecretsalt') {

      watchdog('simplesamlphp_idp', t('Error secretsalt in simplesamlphp config.php is still the default value, this must be changed before authentication will work'));
      return;

    }

  } else {

    watchdog('simplesamlphp_idp', t('Error mission configuration value: please set a value for secretsalt in config.php'));
    return;

  }



  //unset the simplesaml configuration object for security reasons
  unset($sspIdpConfig);

  //create simplesamlphp authentication configuration object to load some configuration values
  $sspIdpAuthConfig = SimpleSAML_Configuration::getConfig('authsources.php');

  if (!is_object($sspIdpAuthConfig)) {
    watchdog('simplesamlphp_idp', t('Error connecting to simplesamlphp: something went wrong while getting the simplesamlphp(idp) authsource configuration.'));
    return;
  }

  //Get config values from configured authentication source in simplesamlphp authsources.php
  $sspIdpAuthConfigValues = $sspIdpAuthConfig->getValue($sspIdpConfigValues['simplesamlphp_idp_auth_source'], NULL);

  //unset the simplesaml authentication configuration object for security reasons
  unset($sspIdpAuthConfig);

  //check if authentication config values have been loaded
  if (!is_array($sspIdpAuthConfigValues)) {
    watchdog('simplesamlphp_idp', t('Error getting simplesamlphp authentication source settings: is the authentication source name used correct and the same in both the drupal module configuration and in simplesamlphp authsources.php?'));
    return;
  }



  //check if the hash_algorithm config value has been set
  if (isset($sspIdpAuthConfigValues['hash_algorithm'])) {

    //check if the config value is not empty
    if (!strlen($sspIdpAuthConfigValues['hash_algorithm'])) {

      watchdog('simplesamlphp_idp', t('Error empty coonfiguration value: please set a value for hash_algorithm in authsources.php'));
      return;

    } else {

      //the hash_algorithm setting has a value
      //obtain the hash algorithm used for the php hash() function
      $sspIdpConfigValues['hash_algorithm'] = $sspIdpAuthConfigValues['hash_algorithm'];
    }

  } else {

    //no value set at all
    watchdog('simplesamlphp_idp', t('Error mission configuration value: please set a value for hash_algorithm in authsources.php'));
    return;

  }


  //check if simplesaml module drupalas is configured to use a bridge cookie
  if (isset($sspIdpAuthConfigValues['use_cookie'])) {

    if (!is_bool($sspIdpAuthConfigValues['use_cookie'])) {

      watchdog('simplesamlphp_idp', t('Error configuration value is not boolean: please set value for use_cookie in authsources.php and make sure it is boolean (so TRUE or FALSE without quotes)'));
      return;

    } else {

      $sspIdpConfigValues['use_cookie'] = $sspIdpAuthConfigValues['use_cookie'];

    }

  } else {

    //this value must always be present as wthout the use of a cookie there are
    //serious security vulnerabilities
    watchdog('simplesamlphp_idp', t('Error missing configuration value: please set a value for use_cookie in authsources.php and make sure it is boolean (so TRUE or FALSE without quotes)'));
    return;

  }



  //if configuration is set to use a cookie load extra config values
  if ($sspIdpConfigValues['use_cookie'] == TRUE) {

    //obtain the cookie name from the simplesamlphp authsources.php file
    if (isset($sspIdpAuthConfigValues['cookie_name'])) {

      if (!strlen($sspIdpAuthConfigValues['cookie_name'])) {

        watchdog('simplesamlphp_idp', t('Error empty configuration value: please set a value for cookie_name in authsources.php'));
        return;

      } else {

        $sspIdpConfigValues['cookie_name'] = $sspIdpAuthConfigValues['cookie_name'];

      }

    } else {

      watchdog('simplesamlphp_idp', t('Error missing configuration value: please set a value for cookie_name in authsources.php'));
      return;

    }



    //check if simplesamlphp module drupalas is configured to use a secure cookie
    if (isset($sspIdpAuthConfigValues['cookie_secure'])) {

      if (!is_bool($sspIdpAuthConfigValues['cookie_secure'])) {

        watchdog('simplesamlphp_idp', t('Error empty configuration value: please set a value for cookie_secure in authsources.php and make sure it is boolean (so TRUE or FALSE without quotes)'));
        return;

      } else {

        $sspIdpConfigValues['cookie_secure'] = $sspIdpAuthConfigValues['cookie_secure'];

        //if configuration is set to use a secure cookie load extra config values
        if ($sspIdpConfigValues['cookie_secure'] == TRUE) {

          //obtain the cookie domain from the simplesamlphp authsources.php file
          if (isset($sspIdpAuthConfigValues['cookie_domain'])) {

            if (!strlen($sspIdpAuthConfigValues['cookie_domain'])) {

              watchdog('simplesamlphp_idp', t('Error empty configuration value: please set a value for cookie_domain in authsources.php'));
              return;

            } else {

              $sspIdpConfigValues['cookie_domain'] = $sspIdpAuthConfigValues['cookie_domain'];

            }

          } else {

            watchdog('simplesamlphp_idp', t('Error missing configuration value: please set a value for cookie_domain in authsources.php (set cookie_secure to false to not use the cookie_domain option).'));
            return;

          }

        }

      }

    } else {

      watchdog('simplesamlphp_idp', t('Error missing configuration value: please set a value for cookie_secure in authsources.php and make sure it is boolean (so TRUE or FALSE without quotes)'));
      return;

    }

  }



  //this is just for convenience, so you know if the drupal_dir value is set correct in simplesamlphp authsources.php

  // obtain the drupal base directory from the simplesamlphp authsources.php file
  // and compare it to the actual drupal directory
  // give error if they dont match
  if (isset($sspIdpAuthConfigValues['drupal_dir'])) {

    if (!strlen($sspIdpAuthConfigValues['drupal_dir'])) {

      watchdog('simplesamlphp_idp', t('Error empty configuration value: please set a value for drupal_dir in authsources.php'));
      return;

    } else if ($sspIdpAuthConfigValues['drupal_dir'] != DRUPAL_ROOT) {

      watchdog('simplesamlphp_idp', t('Error wrong configuration value: drupal_dir in authsources.php does not match the directory of this drupal site.'));
      return;

    }

  } else {

    watchdog('simplesamlphp_idp', t('Error missing configuration value: please set a value for drupal_dir in authsources.php'));
    return;

  }



  //return the configuration values
  return $sspIdpConfigValues;

}